#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2005-2019 Centre National d'Etudes Spatiales (CNES)
#
# This file is part of Let-it-snow (LIS)
#
#     https://gitlab.orfeo-toolbox.org/remote_modules/let-it-snow
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import json
import logging
import argparse
import os
import sys

snow_annual_map_json_template = {
    "log": True,
    "mode": "RUNTIME",
    "use_densification": False,
    "date_margin": 15,
    "ram": 4096,
    "nb_threads": 6,
}


def main():
    # Parse arguments
    parser = argparse.ArgumentParser(description='This script is used to \
                                generate the snow annual map configuration json file.')
    parser.add_argument("tile_id", help="The identifier of the tile corresponding \
                                             to the input_products_list (mandatory)")
    parser.add_argument("input_products_list", help="Input products list, containing the paths to homogeneous \
                                                         snow products only on tile_id at same resolution \
                                                         and size (mandatory)")
    parser.add_argument("path_out", help="Path to output directory. (mandatory)")
    parser.add_argument("date_start", help="Start of the date range for which we want to generate \
                                            the snow_annual_map (DD/MM/YYYY) (mandatory)")
    parser.add_argument("date_stop", help="Stop of the date range for which we want to generate \
                                        the snow_annual_map (DD/MM/YYYY) (mandatory)")
    parser.add_argument("--disable_log", help="Disable logging", action="store_true")
    parser.add_argument("--log_stdout", help="Log output (std***.log). (optional, default is path_out/log_stdout.log)")
    parser.add_argument("--log_stderr", help="Log error (std***.log). (optional, default is path_out/log_sterr.log)")
    parser.add_argument("--debug", help="Enable debug mode", action="store_true")
    parser.add_argument("--path_tmp", help="Path where to store temporary files, (optional, default is path_out/tmp)")
    parser.add_argument("--date_margin", help="The margin outside the date range to use for better interpolation results \
                                            (in days) (optional)")
    parser.add_argument("--use_densification", help="Activate the densification using snow products from \
                                                   heterogeneous sensors", action="store_false")
    parser.add_argument("--densification_products_list", help="The densification list, containing the paths to \
                                                             heterogenous snow products from heterogeneous sensors \
                                                             (optional)")
    parser.add_argument("--ram", help="Maximum number of RAM memory used by the program. (optional)")
    parser.add_argument("--nb_threads", help="Maximum number of threads use by the program. (optional)")
    parser.add_argument("--output_dates_filename", help="Output dates list. (optional)")

    args = parser.parse_args()

    # Check mandatory arguments
    if args.tile_id is None:
        logging.error("tile_id is a mandatory argument")
        show_help()
    if args.input_products_list is None:
        logging.error("input_products_list is a mandatory argument")
        show_help()
    if args.path_out is None:
        logging.error("path_out is a mandatory argument")
        show_help()
    if args.date_start is None:
        logging.error("date_start is a mandatory argument")
        show_help()
    if args.date_stop is None:
        logging.error("date_stop is a mandatory argument")
        show_help()
    if args.use_densification and args.densification_products_list is None:
        logging.error("densification_products_list is a mandatory argument when use_densification is true")
        show_help()

    # retrieve template
    jsonData = snow_annual_map_json_template

    # set mandatory arguments
    jsonData["tile_id"] = args.tile_id
    jsonData["input_products_list"] = args.input_products_list.replace("[","").replace("]","").split(',')
    jsonData["date_start"] = args.date_start
    jsonData["date_stop"] = args.date_stop
    jsonData["path_out"] = args.path_out
    path_out = os.path.abspath(args.path_out)
    if not os.path.exists(path_out):
        logging.info("Create directory " + path_out + "...")
        os.makedirs(path_out)

    # overwrite template default values
    if args.disable_log is not None:
        jsonData["log"] = False
    if args.log_stdout is not None:
        jsonData["log_stdout"] = args.log_stdout
    if args.log_stderr is not None:
        jsonData["log_stderr"] = args.log_stderr
    if args.debug is not None:
        jsonData["mode"] = "DEBUG"
    if args.path_tmp is not None:
        jsonData["path_tmp"] = args.path_tmp
    if args.date_margin is not None:
        jsonData["date_margin"] = int(args.date_margin)
    if args.use_densification is not None:
        jsonData["use_densification"] = True
    if args.densification_products_list is not None:
        jsonData["densification_products_list"] = args.densification_products_list.replace("[","").replace("]","").split(',')
    if args.ram is not None:
        jsonData["ram"] = args.ram
    if args.nb_threads is not None:
        jsonData["nb_threads"] = args.nb_threads
    if args.output_dates_filename is not None:
        jsonData["output_dates_filename"] = args.output_dates_filename

    jsonFile = open(os.path.join(path_out, "snow_annual_map.json"), "w")
    jsonFile.write(json.dumps(jsonData, indent=4))
    jsonFile.close()


def show_help():
    print("help !!! :) ")


if __name__ == "__main__":
    # Set logging level and format.
    logging.basicConfig(stream=sys.stdout, level=logging.INFO, format= \
        '%(asctime)s - %(filename)s:%(lineno)s - %(levelname)s - %(message)s')
    main()
