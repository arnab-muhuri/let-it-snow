# Let-it-snow
## Synopsis

This code implements the snow cover extent detection algorithm LIS (Let It Snow) for Sentinel-2, Landsat-8 and SPOT4-Take5 data. It also implements different temporal syntheses based on time series of snow products. 

The algorithm documentation with examples is available here:

* [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.1414452.svg)](https://doi.org/10.5281/zenodo.1414452)

Access to Theia Snow data collection:

* [![DOI:10.24400/329360/f7q52mnk](https://zenodo.org/badge/DOI/10.24400/329360/f7q52mnk.svg)](http://doi.org/10.24400/329360/f7q52mnk)

How to cite:

*  Gascoin, S., Grizonnet, M., Bouchet, M., Salgues, G., and Hagolle, O.: Theia Snow collection: high-resolution operational snow cover maps from Sentinel-2 and Landsat-8 data, Earth Syst. Sci. Data, 11, 493–514, [https://doi.org/10.5194/essd-11-493-2019](https://doi.org/10.5194/essd-11-493-2019), 2019.


The input files are Sentinel-2 or Landsat-8 level-2A products from the [Theia Land Data Centre](https://theia.cnes.fr/) or [SPOT-4/5 Take 5 level-2A products](https://spot-take5.org) and a Digital Terrain Model (DTM). The output is a Level-2B snow product.

The syntheses are temporally aggregated (level-3A) products derived from individual snow products after gapfilling. The three products are: the snow cover duration, the snow disappearance date and the snow appearance date. These products are typically computed over a hydrological year (more details : [Snow cover duration map](doc/snow_annual_map.md).

## Usage
### Snow detector

Run the python script run_snow_detector.py with a json configuration file as unique argument:

```bash
python run_snow_detector.py param.json
```
The snow detection is performed in the Python script [run_snow_detector.py](app/run_snow_detector.py).

All the parameters of the algorithm, paths to input and output data are stored in the json file. The JSON schema (snow_detector_schema.json) is available in the [Algorithm Theoretical Basis Documentation](doc/atbd/ATBD_CES-Neige.tex) and gives more information about the roles of these parameters.

NB: To build DEM data download the SRTM files corresponding to the study area and build the .vrt using gdalbuildvrt. Edit config.json file to activate preprocessing : Set "preprocessing" to true and set the vrt path.

Warning : DEM with nodata value could alter snow detection. zs should be contained between [-431, 8850].

### Snow syntheses

Run the python script run_snow_annual_map.py with a json configuration file as unique argument:
```bash
python run_snow_annual_map.py param.json
```
The snow syntheses are performed in the Python script [run_snow_annual_map.py](app/run_snow_annual_map.py).

All the parameters of the algorithm, paths to input and output data are stored in the json file. The JSON schema (snow_annual_map_schema.json) and its description are available in the [readme](doc/snow_annual_map.md).

## Products format

### Snow product

* SNOW_ALL: Binary mask of snow and clouds.
  * 1st bit: Snow mask after pass1
  * 2nd bit: Snow mask after pass2
  * 3rd bit: Clouds detected at pass0
  * 4th bit: Clouds refined  at pass0
  * 5th bit: Clouds initial (all_cloud)
  * 6th bit: Slope flag (optional 1: bad slope correction)

For example if you want to get the snow from pass1 and clouds detected from pass1 you need to do:
```python
pixel_value & 00000101
```
* SEB: Raster image of the snow mask and cloud mask.
  * 0: No-snow
  * 100: Snow
  * 205: Cloud including cloud shadow
  * 255: No data
* SEB_VEC: Vector image of the snow mask and cloud mask. Two fields of information are embedded in this product. SEB (for Snow Extent Binary) and type.
  * SEB field :
     * 0: No-snow
     * 100: Snow
     * 205: Cloud including cloud shadow
     * 255: No data
     
### Snow syntheses 

Each product is computed for a given tile [TILE\_ID] and a given period from [DATE\_START] to [DATE_STOP]. Products are identified by a tag according the following naming convention: [TILE\_ID]\_[DATE\_START]\_[DATE_STOP]

For example: **T31TCH\_20170901\_20180831**

LIS generates the following files:
- Raster: **DAILY\_SNOW\_MASKS\_<*tag*>.tif**, the snow time series file interpolated on a daily basis (1 image with one band per day). Each band are coded as follows (the interpolation removing any clouds or nodata):
	- 0: No-snow
	- 1: Snow

- Raster: **SCD\_<*tag*>.tif**, the snow cover duration map (SCD), pixel values within [0-number of days] corresponding the number of snow days.

- Raster: **CLOUD\_OCCURENCE\_<*tag*>.tif**, the cloud/nodata annual map image, pixel values within [0-1] corresponding the cloud or nodata occurrences in the non-interpolated time series

- Raster: **SMOD\_<*tag*>.tif**, the date of snow disappearance (Snow Melt-Out Date), defined as the last date of the longest snow period. The dates are given in number of days since the first day of the synthesis.

- Raster: **SOD\_<*tag*>.tif**, the date of snow appearance (Snow Onset Date), defined as the first date of the longest snow period. The dates are given in number of days since the first day of the synthesis.

- Raster: **NOBS\_<*tag*>.tif**, the number of clear observations to compute the SCD, SMOD and SOD syntheses

Output directory will also contain the following files :

- Text file: **input_dates.txt**, the list of observation dates in the non-interpolated time series
- Text file: **output_dates.txt**, the list of interpolated dates

- JSON file: **param.json**, the configuration file used for the products generation (optional)

- LOG file: **stdout.log**, the log file for the standard output generated during processing (optional)

- LOG file: **stderr.log**, the log file for the error output generated during processing (optional)


## Data set example

Sequence of snow maps produced from Sentinel-2 type of observations (SPOT-5 Take 5) over the Deux Alpes and Alpe d'Huez ski resorts are available on [Zenodo](http://doi.org/10.5281/zenodo.159563).

## Motivation

Code to generate the snow cover extent product on Theia platform.

## Installation

LIS processing chain uses CMake (http://www.cmake.org) for building from source.

### Dependencies

Following a summary of the required dependencies: 

* GDAL >=2.0
* OTB >= 7.0
* Python interpreter >= 3.6
* Python libs >= 3.6
* Python packages:
* numpy
* lxml
* matplotlib
* rasterio

GDAL itself depends on a number of other libraries provided by most major operating systems and also depends on the non standard GEOS and Proj libraries. GDAL- Python bindings are also required

Python package dependencies:

* sys
* subprocess
* glob
* os
* json
* gdal

Optional dependencies:

* gdal_trace_outline can be used alternatively to gdal_polygonize.py to generate the vector layer. It requires to install [dans-gdal-scripts utilities](https://github.com/gina-alaska/dans-gdal-scripts).

### Installing from the source distribution

#### General

In your build directory, use cmake to configure your build.
```bash
cmake -C config.cmake source_lis_path
```
In your config.cmake you need to set :
```bash
LIS_DATA_ROOT
```
For OTB superbuild users these cmake variables need to be set:
```bash
OTB_DIR
ITK_DIR
GDAL_INCLUDE_DIR
GDAL_LIBRARY
```
Run make in your build folder.
```bash
make
```
To install let-it-snow application and the s2snow python module.
In your build folder:
```bash
make install
```

Add appropriate executable rights
```bash
chmod -R 755 ${install_dir}
```

The files will be installed by default into /usr/local and add to the python default modules.
To overrsouride this behavior, the variable CMAKE_INSTALL_PREFIX must be configure before build step.

Update environment variables for LIS. Make sure that OTB and other dependencies directories are set in your environment variables:
```bash
export PATH=/your/install/directory/bin:/your/install/directory/app:$PATH
export LD_LIBRARY_PATH=/your/install/directory/lib:$LD_LIBRARY_PATH
export OTB_APPLICATION_PATH=/your/install/directory/lib:$OTB_APPLICATION_PATH
export PYTHONPATH=/your/install/directory/lib:/your/install/directory/lib/python3.7/site-packages:$PYTHONPATH
```
let-it-snow is now installed.

## Tests

Enable tests with BUILD_TESTING cmake option. Use ctest command to run tests. Do not forget to clean your output test directory when you run a new set of tests.

Data (input and baseline) to run validation tests are available on Zenodo:

* [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.166511.svg)](https://doi.org/10.5281/zenodo.166511)

Download LIS-Data and extract the folder. It contains all the data needed to run tests. Set Data-LIS path var in cmake configuration files.
Baseline : Baseline data folder. It contains output files of S2Snow that have been reviewed and validated.
Data-Test : Test data folder needed to run tests. It contains Landsat, Take5 and SRTM data.
Output-Test : Temporary output tests folder.
Do not modify these folders.

## Contributors

Manuel Grizonnet (CNES), Simon Gascoin (CNRS/CESBIO), Germain Salgues (Magellium), Aurore Dupuis (CNES), Rémi Jugier (Magellium)

## License

This is software under the Apache License v2.0. See https://www.apache.org/licenses/LICENSE-2.0.txt
